\documentclass[a4paper, 12pt]{article}
\usepackage{pgfplots}
\usepackage{color}
\usepackage{inconsolata}
\usepackage{commath}
\usepackage{listings}
\usepackage[T1]{fontenc}
% \usepackage[procnames]{listings}
% \usepackage{tikz}
% \usepackage{verbatim}
\usepackage[utf8]{inputenc}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{times}
\usepackage{caption}
\captionsetup{font=footnotesize}
\captionsetup{justification=centering,margin=4cm}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\newcommand{\maxatop}[2]{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily #2}}}{#1}}}
\newcommand{\var}[1]{\textnormal{Var}(#1)}
\newcommand{\cov}[1]{\textnormal{Cov}(#1)}
\newcommand{\red}[0]{\color{red}}
\newcommand{\blue}[0]{\color{blue}}
\newcommand{\darkgreen}[0]{\color{darkgreen}}
\newcommand{\argsinh}[1]{\textnormal{Argsinh}#1}
\newcommand{\tr}[1]{\textnormal{Tr}\left(#1\right)}
\newcommand{\ke}[1]{\textnormal{Ker}\left(#1\right)}
\newcommand{\im}[1]{\textnormal{Im}\left(#1\right)}
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}
\definecolor{darkgreen}{RGB}{0,200,0}
\def\blankpage{%
  \clearpage%
  \thispagestyle{empty}%
  \addtocounter{page}{-1}%
  \null%
  \clearpage}
% \newcommand{\choice}[2]{\begin{pmatrix}#1 \\ #2\end{pmatrix}}
% \newcommand\norm[1]{\left\lVert#1\right\rVert} % requires amsmath
\usepackage{gensymb}
\usepackage{wasysym}
\usepackage{siunitx}
% \usepackage{amsfonts}
% \usepackage{wrapfig}
\usepackage{amssymb}
\usepackage{marvosym}
\usepackage{polynom}
% \usepackage{mathtools}
\usepackage{amsmath}
\usepackage[parfill]{parskip}
% \DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\usetikzlibrary{trees}
% \usetikzlibrary{quotes,angles,calc}
% \usetikzlibrary{arrows}
\textheight = 250mm
\topmargin = -2cm
\textwidth = 175mm
\oddsidemargin = -0.75cm
% \tikzset{
%     right angle quadrant/.code={
%         \pgfmathsetmacro\quadranta{{1,1,-1,-1}[#1-1]}     % Arrays for selecting quadrant
%         \pgfmathsetmacro\quadrantb{{1,-1,-1,1}[#1-1]}},
%     right angle quadrant=1, % Make sure it is set, even if not called explicitly
%     right angle length/.code={\def\rightanglelength{#1}},   % Length of symbol
%     right angle length=2ex, % Make sure it is set...
%     right angle symbol/.style n args={3}{
%         insert path={
%             let \p0 = ($(#1)!(#3)!(#2)$) in     % Intersection
%                 let \p1 = ($(\p0)!\quadranta*\rightanglelength!(#3)$), % Point on base line
%                 \p2 = ($(\p0)!\quadrantb*\rightanglelength!(#2)$) in % Point on perpendicular line
%                 let \p3 = ($(\p1)+(\p2)-(\p0)$) in  % Corner point of symbol
%             (\p1) -- (\p3) -- (\p2)
%         }
%     }
% }
\title{How do you calculate the distance to a predetermined object
  using two cameras in a stereoscopic layout?}
\author{Max Generowicz}
\begin{document}
\begin{titlepage}
   \begin{center}
       \vspace*{0.2cm}
       \Huge
       \textbf{How do you calculate the distance to a predetermined
         object using two cameras in a stereoscopic layout?}

       \vspace*{1cm}

       \includegraphics[width=0.9\textwidth]{title.png}

       \vspace*{1cm}

       \Large
       Maturité project by \\
       \vspace*{0.5cm}
       \Large
       \textbf{GENEROWICZ Max (4B)}

       \vspace*{1.3cm}

       \textbf{Project Supervisor: Thomas Speer} \\

       \vspace*{0.9cm}
       \large
       COLLEGE VOLTAIRE \\
       GENEVE \\

       \vspace*{0.3cm}
       \normalsize
       2019

   \end{center}
 \end{titlepage}
 \blankpage
 \tableofcontents
 \newpage
 \addtocontents{toc}{\protect}
\section{Introduction}
For my maturité project, I have decided to do a project about computer
vision. Computer-based technology has been increasing in importance in
our lives over the last few decades and will no doubt continue do so
in the future, so I think a project like this is very relevant to my
education, especially as I plan on studying sciences.

The development of this project can be seen as a three-stage process:
preparation of hardware, tracking and distance calculation.

The hardware section of my project consists of learning to use the
different hardware components, as well as selecting and combining them
to create a working whole. Before this project, my studies of
mathematics and physics were predominantly theoretical, with
experiments being done in highly idealised environements, offering
little scope for confrontation with the complications of the real
world. An appealing aspect of this project for me is to get more
insight into dealing with the divergence between theory and practice.

The tracking section of my project consists of detecting an object of
a given colour in the video feed of two cameras and tracking it
throughout the video stream. This aspect of the project is especially
interesting to me since the problems that have to be solved in order
for detection to work are problems which approach the realm of machine
learning. Machine learning is a subject which has recently gained a
lot of popularity and which I hope to be able to learn more about one day.

The distance calculating section of the project consists of using the
position of the object in each camera view to compute its distance to
the camera plane. This aspect of the project introduced me to 3D
computer vision, the branch of computer science which deals with the
reconstruction of structures of scenes using images taken from
different points of view. Interestingly, while learning the theory
necessary for this I discovered a new topic in maths that I had never
studied before: projective geometry.

In this written document, I will present the tools I used to complete
my project, then explain the process that I went through to get to the
final working prototype, explaining the theory necessary along the
way. Finally, I will conclude with my personal experience and possible
continuation of the project.
\newpage
\section{Tools}
In this section, I will describe all the tools I used to complete this
project, be they hardware or software tools.
\subsection{Raspberry Pi}
The main hardware element I used for this project was a Raspberry
Pi. Raspberry Pis are small, cheap and versatile computers which have
gained immense popularity across a wide range of applications, from
hobbyist projects to use in commercial products.

In the opening stages of my project, I used the Raspberry Pi 3 Model
B+, which includes an SD card reader, 4 USB ports, an ethernet port,
an HDMI port and a micro-USB port for power, allowing me to connect a
USB wireless keyboard, USB mouse, WiFi dongle, and a monitor to it;
essentially making a fully-fledged (albeit not particularly powerful)
computer. This was adequate for the two stages of the project:
installing and verifying the libraries for the use of a single camera
and developing the first prototype programs (those requiring only one
camera) including tracking. However, the distance calculation part of
the project requires simultaneous input from two cameras, and this
particular model of the Raspberry Pi only has one, so I had to
upgrade.

I ended up using the Raspberry Pi Compute Module 3, which is
essentially the computational core of a Raspberry Pi without onboard
I/O interfaces. The lack of (bulky) I/O interfaces is an advantage
from the perspective of obtaining a small final product, but it makes
interacting with it during development very difficult (this would
require soldering wires). To make development easier, one can
temporarily slot the Compute Module into an \textit{I/O board}, which
provides various I/O interfaces: an SD card reader, one USB port, an
HDMI port, a micro-USB port for power and, most importantly for my
project, \textit{two} camera inputs.

An added benefit of working with the Compute Module is that once
satisfied with the programs on it, I could easily embed it in some
kind of robot by detaching it from the I/O board (the dimensions of
the Compute Module are 3.5cm \(\times\) 7cm).
\subsection{Raspberry Pi Camera Module}
Given that my project is about a stereoscopic camera, I needed a cheap
and efficient way of connecting cameras to my Raspberry Pi. Luckily,
the Raspberry Pi company provide small, simple, Raspberry Pi-compatible
cameras.

In this project I used the newer V2 model (released in 2016). These
cameras provide many benefits: just like the Raspberry Pi, they are
cheap, small, simple and therefore very versatile. On a more technical
note, the cameras can nearly be considered as pinhole cameras,
therefore distortion (extremely visible in GoPro cameras for example)
is insignificant. This in turn facilitates the calibration process
because the undistortion algorithm produces negligeable results,
meaning that this step can be skipped altogether (this will be
expanded upon later).

I created the stereoscopic camera by fixing two of these cameras on a wooden board.
\subsection{OpenCV}
OpenCV (Open Source Computer Vision), as the name suggests, is an open
source library dedicated to computer vision, which is the field of
computer science that focuses on computers processing and analysing
images or videos.

In my case, OpenCV comes into play whenever I have to track some
object in the frame of the video feed. Additionally, OpenCV implements
various \textit{stereo} algorithms, needed for calibrating cameras, and
groups them together into higher-level functions with convenient
interfaces. These functions remove the tedious task of coordinating these
algorithms, thus greatly simplifying the code and reducing the scope
for mistakes.
\subsection{Imutils}
Imutils is a rather minor tool in the whole project, but it gives me
the opportunity to talk about its creator, who has helped me
immensely in this project. Imutils is a ``collection of OpenCV
convenience
functions''\footnote{ROSEBROCK Adrian (Pyimagesearch),\, \textbf{Ball Tracking with
  OpenCV [online]}, \\
https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv
(website visited on 21st February 2019)}
developed by Adrian Rosebrock, founder of Pyimagesearch, a blog which
focuses on computer vision and deep learning. The whole tracking part
of the project is heavily based on material learnt from his blog.

Most image manipulation operations (like resizing an image for
example) require 2 or more steps to complete in OpenCV. Much like the
\textit{stereo} functions of OpenCV condense all the calibration steps
into one function, the imutils functions condense all the OpenCV
functions used for, say, resizing an image into one much more
user-friendly \lstinline{resize} function, thus making the code neater
and easier to understand.
\subsection{Python}
Python is a general-purpose programming language which has found a lot
of success in scientific and technical circles for two main reasons:
firstly, it is a high-productivity language, meaning that complex
ideas can be expressed in it relatively easily; secondly, it is
possible to wrap libraries written in low-level languages and make
them available as Python modules.

This offers the best of both worlds: optimisation of programmer time
as well as CPU time.

In the context of this project, I could use OpenCV's computational
engine (written in C++) in Python.
\subsection{Additional tools}
It goes without saying that all of these hardware and software tools have
to be connected in some way or another. This is a list of those minor
components which connect the main components:
\begin{enumerate}[-]
\item HDMI cable: used to connect the Raspberry Pi to the monitor
\item GPIO jumper wires: used to connect the GPIO pins on the
  Raspberry Pi to allow multiple camera use
\item Raspberry Pi Camera Cable: used to connect the cameras to the
  Raspberry Pi
\item Micro-USB cable: used to power the Raspberry Pi
\item USB wireless keyboard: used as keyboard input to the
  Raspberry Pi (Logitech K360)
\item USB mouse: used as mouse input to the Raspberry Pi (Trust
  compact mouse)
\item WiFi dongle: used to give the Raspberry Pi access to WiFi (Zyxel
  Wireless USB adaptor NWD2205)
\item USB hub: used to connect the previous 3 elements to the
  Raspberry Pi (Icybox IB-AC6401)
\item Raspbian: operating system used on the Raspberry Pi
\item SD card: card on which the operating system was installed
\end{enumerate}

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.5]{setup.png}}
  \center{\caption{General overview of the hardware components:
      Raspberry Pi, PiCameras, USB mouse, GPIO cables.}\label{fig:setup}}
\end{figure}
\newpage
\section{Process}
In this section, I will describe the steps which led me from bare components to a working model.
\subsection{Setup}
Setting up the Raspberry Pi with all the required software turned out
to be the more work than expected. Here is the general outline of the
steps performed to obtain a working Raspberry Pi on which I could
comfortably work on my project (the exact procedure is slightly more
complicated and quite technical, so I will just outline the major
stages):

First of all, I installed Raspbian (an operating system based on
Debian Linux and developed by the Raspberry Pi developer team) on the
SD card. To do this, I followed the Raspberry Pi developer team's
official instructions. In summary, this involved writing the Raspbian
image to the SD card, which could then be inserted into the Raspberry
Pi. The Raspberry Pi now had a working operating system.

Next, I configured the operating system to allow it to connect to the
Internet via WiFi, to change the hostname and various other
details. Most importantly, this step allowed me to enable the
camera. At this point, I had a Raspberry Pi with a working camera and
I could take pictures using the \lstinline{raspistill} command.

I now had to install my main software tool, OpenCV. This was much more
complicated than anticipated, as it involved installing a many
additional packages (such as \lstinline{numpy}, Python's workhorse for
numerical libraries), creating a virtual environnement for OpenCV to
live in, increasing the swap space to allow for the compilation of
OpenCV, before finally being able to install OpenCV itself by
compiling it from source, this final step alone taking a couple of
hours of computing time.

Finally, I installed all the other software tools that I have already
talked about, namely \lstinline{imutils}, \lstinline{picamera} (which
\lstinline{imutils} requires) and \lstinline{IPython}.

I now had a working operating system on the Raspberry Pi, complete
with all of the software tools I would need. In addition to that,
during the configuration of Raspbian, I had enabled the SSH server
which now allowed me to connect to the Raspberry Pi directly from my
computer. This was crucial going forward as it allowed me write files
on the Raspberry Pi while typing at my computer, which made the
interaction with the Raspberry Pi much more comfortable and efficient.
\subsection{Tracking}
In this section, I will explain the details of tracking the ball in
the video feed and present an overview of how the code works. The
program I used was inspired by Adrian Rosebrock of
Pyimagesearch's blog post: \textit{Ball Tracking with OpenCV}
\footnote{ROSEBROCK Adrian (Pyimagesearch),\, \textbf{Ball Tracking with
  OpenCV [online]}, \\
https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv
(website visited on 21st February 2019)}.

Tracking an object in a video requires identifying that object's
position in each frame of the video. I will therefore explain how my
code detects an object in an image.

First, the program prepares the image for masking. This is a procedure
that has been extensively studied and optimised and it is clear that
much better and more precise versions of this algorithm exist, but for
the precision that I need, I can afford to use a simpler, quicker
version. It simply involves resizing the image and slightly blurring
it. This is done to smooth out the image and reduce noise, which makes
it easier to analyse.

After this, the image is masked. The mask is simply an indicator as to
what pixels of the image interest us: the \textit{uninteresting}
pixels will be black and the \textit{interesting} ones will be
white. Therefore, the mask of an image containing a ball we want to
track will look like a black image with a white circle in it. This
mask is then dilated and eroded to eliminate slight imperfections
(even though this step is technically optional, it helps the rest of
the program to work faster). The mask is computed by an OpenCV
function called \lstinline{cv2.inRange}, which turns any pixel whose
colour is in the specified colour range (in my case a yellowish green
because I am using a tennis ball) white, thus creating the
aforementioned mask.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.3]{masking.png}}
  \center{\caption{On the left, the source image; on the right, the mask created from that image.}\label{fig:masking}}
\end{figure}

Next, the appropriately named \lstinline{cv2.findContours} finds the
contours of the mask, which in our image represents the edge of the
ball. From this contour, the program finds the smallest shape that
encloses the contour (once again this is done by an OpenCV
function). To simplify matters, I have given the user two choices for
the enclosing shape: a circle or a rectangle (the functions used are
\lstinline{cv2.minEnclosingCircle} and \lstinline{cv2.minAreaRect}
respectively). A simple function then calculates the centre of the
enclosing shape, which, to a first-order approximation, gives pixel
coordinates of the centre of mass of the object on the frame.

The pair of frames (from the left and right cameras) is processed to
yield the centre coordinates of the object of interest on each image
and this process is immediately repeated as soon as this calculation
is complete.
\subsection{Pre-calibration}
We come now to the more technical and complicated part of the
project. For all sections on calibration, I will try to explain the
process as precisely as possible without diving into the exact
mathematics of it, as that would make these explanations extremely
long.

In order to compensate for imperfections in the cameras, we need to
find and quantify these imperfections. This process is called
calibration. The lens plane not being parallel to the image plane is
just one example of the various imperfections which will inevitably
arise when using such small and precise equipment.

To do this, we need to introduce two new concepts: the camera
intrinsics and lens distortion.

Camera intrinsics are properties of the camera and are described by 4
values, named \(f_x\), \(f_y\), \(c_x\) and \(c_y\). \(f_x\) and
\(f_y\) describe the focal lengths in the \(x\) and \(y\) directions
(which in cheap cameras tends to differ). \(c_x\) and \(c_y\)
represent the coordinates of the centre of the image with respect to
the centre of projection (again, these two centres will never
perfectly align, due to tiny manufacturing imprecisions). These values
are stored in the \textbf{camera intrinsics matrix} (simply called
\textit{camera matrix}) as follows:
\[M=\begin{pmatrix}f_x & 0 & c_x \\ 0 & f_y & c_y \\ 0 & 0 &
    1\end{pmatrix}\] The advantage of presenting the camera intrinsics
like this is that the projection (\(p\)) onto the image plane of any
point (\(P\)) can be expressed as a matrix operation: \[p=MP\]

Lens distortion is easier to visualise than to explain. Everybody has
seen footage from a fish-eye lens, with straight lines near the edges
of the image becoming curved \footnote{Image from https://help.shopmoment.com/article/181-superfish-distortion-correction}.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.25]{distortion.png}}
  \center{\caption{An example of radial distortion: straight lines in
      the real world appear curved on the image.}\label{fig:distortion}}
\end{figure}

In reality, this is due to what is called \textbf{radial distortion},
i.e. the further objects are from the centre of the image, the more
curved they are. Put simply, this is due to the fact that the lens
bends the light rays that are further from the centre more than it
should. A less visible type of distortion is \textbf{tangential
  distortion}. This type of distortion results in a slight squashing
of the objects in the image and is due to the lens not being parallel
to the image plane (the plane where the image forms, for example the
photosensitive paper in old cameras). These distortions are described
by the \textbf{distortion vector}
\[\begin{pmatrix}k_1 \\ k_2 \\ p_1 \\ p_2 \\ k_3\end{pmatrix}\] with
\(k_1\), \(k_2\) and \(k_3\) describing radial distortion (\(k_3\)
being used for extreme fish-eye lenses) and \(p_1\) and \(p_2\)
describing tangential distortion.

The goal of our calibration will be to find the camera intrinsics matrix and
the distortion vector.
\subsection{Single camera calibration}
Now that we know what we are looking for, we can examine how to look
for it. The process of finding the camera matrix and the distortion vector
is relatively simple to understand, even though the mathematics behind
it is rather long and complicated.

In essence, we want to take pictures of objects of regular and
well-understood structure, from many different positions and
orientations relative to the camera. As we know what properties the
object \textit{should} have and we can see what the result of taking a
picture of that object with the camera is, we can deduce the
transformations that turned the object in 3D space into the image we
have. Hidden in those transformations are our values of \(f_x\),
\(f_y\), \(k_1\), etc. (and that is where the mathematics comes into
play!). The perfect object for this task is a chessboard. We know that
a chessboard is made of equal sized squares, so by finding the corners
of the chessboard on the image (a very easy task accomplished by a
simple OpenCV function, \lstinline{cv2.findChessboardCorners}), we
know that the corresponding points in 3D space are all at the same
distance from each other. Using some projective geometry, we can find
the exact values for all of our variables. There is, of course, an
OpenCV function that does this for us:
\lstinline{cv2.calibrateCamera}. This function takes a number of
images of a chessboard in different orientations (I used 10) and
outputs the camera intrinsics matrix and the distortion coefficients,
in addition to the position of the chessboard in each image with
respect to the camera expressed as a rotation matrix and a translation
vector, but these last two outputs don't interest us all that much.
\subsection{Stereo calibration and rectification}
After obtaining the necessary information about each camera
individually, it is time to calibrate the cameras together and adjust
for small manufacturing and configuration imprecisions that could
arise. These two steps are called stereo calibration and stereo
rectification.
\subsubsection{Stereo calibration}
Before moving forward, I will introduce some terminology. A camera is
defined by a centre of projection \(O\) and an image plane
\(\Pi\). Given a point \(P\) in 3D space, the position on the screen where
we see \(P\) will be determined by the intersection of the line \(OP\)
and the image plane \(\Pi\). This intersection point we call
\(p\). Next we define the principal ray, which is the ray
perpendicular to \(\Pi\) passing through \(O\). The intersection of the
principal ray with \(\Pi\) is the principal point \(c\). It is
important to observe that \(c\) does not always coincide with the
centre of the screen.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.45]{terminology.png}}
  \center{\caption{Overview of a stereo camera setup.}\label{fig:camerarottrans}}
\end{figure}

The aim of stereo calibration is to express the position of one camera
with respect to the other, as this then helps to precisely rectify the
camera setup mathematically rather than physically.

As seen in Figure~\ref{fig:camerarottrans} below,
this position can be expressed as a translation vector and a rotation
matrix.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.45]{camerarottrans.png}}
  \center{\caption{The position of one camera with respect to the other camera
    can be expressed as a translation vector and a rotation matrix.}\label{fig:camerarottrans}}
\end{figure}

This information is contained in the essential matrix \(E\) and the
fundamental matrix \(F\). The difference between these two matrices is
that \(F\) also contains the camera intrinsics. More precisely, these
matrices can relate the projection of a point \(P\) on one camera to
the projection of \(P\) on the other camera. As \(F\) contains
information about the cameras, it relates the pixel coordinates of the
two cameras. Thus, given a point \(P\) in space and its projection
onto the pixels \(p_l\) and \(p_r\) (onto the left and right cameras),
the equation \[p_r^{\mathsf{T}} Fp_l=0\] holds\footnote{\(q_l\) and \(q_r\)
  are 3 \(\times\) 1 vectors as they are expressed in homogeneous
  coordinates. A 2D point expressed in homogeneous coordinates is
  written as \(\begin{pmatrix}x & y & w\end{pmatrix}\), with the
  \textit{real} pixel coordinates being \(\begin{pmatrix}x/w &
    y/w\end{pmatrix}\)}.

In other words, if we have two points \(p_r\) and \(p_l\) which we
know are projections of the same 3D point \(P\) onto both cameras,
then we can calculate the fundamental matrix \(F\). Using this, it is
possible to calculate the relative position of the cameras, under the
condition that we have a series of images taken from both cameras
which contain points of which we know the relative position. Once
again, OpenCV will use chessboards to do
this. \lstinline{cv2.stereoCalibrate} takes pairs of images of
chessboards (one image per camera) is input, as well as the intrinsics
for each camera (distortion coefficients and camera matrix). The
output of this function is the rotation matrix and translation vector
between the two cameras and the essential and fundamental matrices.
\subsubsection{Stereo rectification}
Calculating the distance from two images is much easier if the cameras
are aligned (i.e. their image planes are coplanar). The goal of stereo
rectification will be to align the two cameras as seen in
Figure~\ref{fig:rectification}.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.45]{rectification.png}}
  \center{\caption{The process of rectification will rectify the image
    planes (mathematically rather than physically) so that they are aligned.}\label{fig:rectification}}
\end{figure}

One of the algorithms that OpenCV uses to do this is Bouguet's
algorithm. In a nutshell, Bouguet's algorithm splits the rectification
matrix in two: rectifying the left camera by half of the rotation and
the right camera by the other half, thereby minimising the change in
each image plane (if the full rotation is done by one single camera,
it amplifies the effect of imprecisions).

OpenCV's \lstinline{cv2.stereoRectify} takes 7 inputs: the camera
matrices and distortion coefficients for both cameras as well as the
image size and most importantly the rotation matrix and translation
vector calculated by \lstinline{cv2.stereoCalibrate}. From these
inputs, it calculates the rotation matrix needed for both cameras to
rectify the image planes. However, it also outputs a projection matrix
for each camera in the rectified coordinate system. This is very
useful for us as a projection matrix \(P\) of a camera is a matrix which projects
3D coordinates onto the image plane of that camera \footnote{Once again both the 3D point and its 2D
projection are in homogeneous coordinates.}. In other
words \[P\begin{pmatrix}X \\ Y \\ Z \\
    1\end{pmatrix}=\begin{pmatrix}x \\ y \\
    w\end{pmatrix}\] The importance of \(P\) is that it projects a
point in 3D space onto the \textit{rectified} image plane, meaning
that the resulting \(\begin{pmatrix}x/w \\ y/w\end{pmatrix}\) will be
the rectified pixel coordinates.

The last output of \lstinline{cv2.stereoCalibrate} that interests us
is the \(Q\) matrix, also called reprojection matrix or
disparity-to-depth matrix. Let \(x_l\) be the \(x\)-coordinate of a
pixel on the left camera, and let \(x_r\) be the corresponding pixel
on the right camera. Then we define the disparity \(d=x_l-x_r\) and we
have \[Q\begin{pmatrix}x_l \\ y_l \\ d \\
    1\end{pmatrix}=\begin{pmatrix}X \\ Y \\ Z \\ W\end{pmatrix}\]
where \(\begin{pmatrix}X/W \\ Y/W \\ Z/W\end{pmatrix}\) is the 3D
point projected onto \(\begin{pmatrix}x_l \\ y_l\end{pmatrix}\) on the
left camera and \(\begin{pmatrix}x_r \\ y_r\end{pmatrix}\) on the
right camera.

The process of calculating the rectified pixel coordinates from the
old pixel coordinates is the following: given the two unrectified
pixel coordinates \(\begin{pmatrix}{x_l}' \\ {y_l}'\end{pmatrix}\)
(for the left camera) and \(\begin{pmatrix}{x_r}' \\
  {y_r}'\end{pmatrix}\) (for the right camera), we calculate the 3D
point they represent using the reprojection matrix \(Q\) given by
\lstinline{cv2.stereoRectify} \[\begin{pmatrix}X \\ Y \\ Z
    \\ W\end{pmatrix}=Q\begin{pmatrix}{x_l}' \\ {x_r}' \\
    {x_l}'-{x_r}' \\ 1\end{pmatrix}\] Then we calculate the rectified
pixels in homogeneous coordinates using the two projection matrices given by \lstinline{cv2.stereoRectify}:
\begin{align*}
  \begin{pmatrix}x_L \\ y_L \\ w_L\end{pmatrix}&=P_l\begin{pmatrix}X/W
    \\ Y/W \\ Z/W \\ 1\end{pmatrix} \\
  \begin{pmatrix}x_R \\ y_R \\ w_R\end{pmatrix}&=P_r\begin{pmatrix}X/W
    \\ Y/W \\ Z/W \\ 1\end{pmatrix} \\
\end{align*}
The final rectified pixel coordinates will
be \[p_l=\begin{pmatrix}x_l \\ y_l\end{pmatrix}\ \ \ ,\ \ \
  p_r=\begin{pmatrix}x_r \\ y_r\end{pmatrix}\ \ \ \ \ \ \
  \textnormal{ with }x_l=\frac{x_L}{w_L}\,,\ y_l=\frac{y_L}{w_L}\,,\
  x_r=\frac{x_R}{w_R}\,,\ y_r=\frac{y_R}{w_R}\]
To summarise this process: \[\textnormal{Unrectified pixel coordinates
  }\maxatop{\longrightarrow}{\(Q\)}\textnormal{ 3D coordinates
  }\ \maxatop{\longrightarrow}{\(P_l\) and \(P_r\)}\ \textnormal{
    Rectified pixel coordinates}\]
\subsubsection{Distance calculation}
Now that we have aligned the image planes of the cameras, we have
reduced the problem of calculating the distance to a much simpler
one. The situation is summarised in Figure~\ref{fig:distancecalculation}.

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.45]{distancecalculation.png}}
  \center{\caption{In the theoretically perfect situation that we have
    created by rectifying the cameras, distance calculation is made trivial.}\label{fig:distancecalculation}}
\end{figure}

It is easy to see that in this situation, the distance of the object
from  the cameras is simply \[Z=\frac{fT}{d}\] with \(f\) being the
focal length (which one can find on the Raspberry Pi website), \(T\)
the distance between the cameras and \(d\) the disparity.
\newpage
\section{Conclusion}
In conclusion, I will briefly talk about my personal experience while
working on this project (including some problems I encountered), as
well as commenting on the results I obtained and a possible
continuation to this project in the future.
\subsection{Personal experience}
For me, this project was a completely new experience, as I have never
worked on a project this big for this long. It is also the first time
that I have worked on a project involving a non-trivial amount of
hardware, and it was one of the main aspects that I had to get used
to.

For the first couple weeks I started reading about stereoscopic cameras and
learning about projective geometry, which is the pillar on which all
of 3D vision is based.

In the beginning, I struggled with the hardware slightly: I didn't
know which cables belonged where, I kept forgetting which elements to
connect to which other elements, etc. Over time, I got used to this
and I now feel quite comfortable with my setup.

The first major hurdle in this project was setting up the Raspberry
Pi's software for it to contain all the libraries I needed. A
significant amount of time at the beginning of the project was spent
on converting a bare Raspberry Pi with no operating system into a
working Raspberry Pi which I could SSH to from my computer and run
Python and OpenCV on. After that I could finally take my first picture
with a single camera. Now that I have gone through this process and
recorded it in a logbook, I am confident that I could prepare a new
Raspberry Pi from scratch in an afternoon, but arriving at a process
that works in the first place required an order of magnitude more
work.

As mentioned in the \textit{Tools} section, the original Raspberry Pi I was
working on only had one camera input, so I had to upgrade to the Compute
Module. However, getting images from two cameras wasn't just a
question of plugging the second camera in! After a lot of testing and
searching on the Internet, I found out that GPIO cables where needed
to connect the GPIO pins on the Raspberry Pi. One reason this took me
a long time to find out is because these GPIO cables are not necessary
when using only one camera.

The tracking section of the project went very smoothly. In addition,
the transition from tracking on one image to tracking on two images
simultaneously was quite simple, which was surprising as I expected it
to be a sizeable challenge!

Finally, one thing that facilitated this project immensely was the
simplicity of the cameras. As mentioned in the \textit{Tools} section,
the Raspberry Pi cameras are extremely simple and can be considered as
pinhole cameras. Having run the distortion algorithm on the two
cameras, the output was very close to 0, meaning that distortion was
irrelevant and the undistortion algorithm could be skipped. This means
that I didn't have to deal with resizing the images (undistortion
creates areas of \textit{void} pixels, meaning that images have to be
cropped) which complicates the calibration and rectification
algorithms somewhat.

\subsection{Results}

\begin{figure}[!htb]
  \center{\includegraphics[scale=0.7]{graph.png}}
  \center{\caption{A graph showing the results obtained by the
      distance calculation.}\label{fig:graph}}
\end{figure}

First of all, the tracking results are quite variable. The ability of
the program to detect the ball varies a lot with lighting
conditions. Once the lighting is correctly adjusted, the program can
follow the ball very well. However, when tracking on both images
simultaneously, the frame rate drops drastically (around 10 fps).

Sometimes, the the detection of the ball does not work properly
(confusing another object for the ball), causing the tracking program
to output wrong pixel values, which in turn disrupts the distance
calculation. However, when the ball is correctly identified by both
cameras, the distance calculated by the program is reasonably accurate
as long as the ball is in the \textit{sweet spot}: 10 to 60 cm away
from the camera. When the object was closer than 10 cm, the object was
too big to be seen in both frames and no readings were
available. Beyond 60 cm, results would diverge rapidly from reality, as
is shown in Figure~\ref{fig:graph}.

I have not had the time to get a better understanding of why this is the case.

\subsection{Continuation}
If I had the opportunity to continue this project for another year (or
two!), there are a number of things that I would be interested in
looking into.

First of all, I would like to get a better understanding of the
reasons of why the results deteriorate so drastically beyond 60 cm.

Secondly, although the tracking program is good enough for my
project, the algorithm it uses is very basic relative to all the
different algorithms that have been studied and developed by computer
scientists. Therefore, one of the first things I would change in my
project would be to make the tracking program much more reliable (and
make it work at a quicker frame rate). This would be a great
opportunity to start learning about machine learning, which is a
fascinating and increasingly important subject in the modern world.

Thirdly, I would look into different cameras, simply to experiment
with different equipment, but also because the Pi cameras are quite
limited (because of their simplicity). More advanced cameras would
give me wider angles, higher definition and a lot of other
benefits. This obviously comes at a cost: more complex algorithms!

Finally, the most ambitious continuation would be to install this
system onto some kind of robot, which could then use the information
to perform certain actions. Throughout the project, the idea of a
table tennis-playing robot has been in the back of my mind, using the
tracking and distance calculating programs to predict the path of the
ball. Another possible application of this project could be something
similar to Mark Rober's incredible moving
dartboard \footnote{ROBER Mark,\, \textbf{Automatic Bullseye, MOVING DARTBOARD [online]}, \\
https://www.youtube.com/watch?v=MHTizZ\_XcUM (website visited on 26th
October 2019)}. Even
though he used motion capture technology for his project, using 3D
computer vision would be an equally valid method of accomplishing
this.
\newpage
\section{Bibliography}
\textbf{BRADSKI Gary} and \textbf{KAEHLER Adrian},\, \textit{Learning
  OpenCV},\, Sebastopol,\, O'Reilly,\, 2008 \\
(All diagrams in this document are taken from this book) \\
\\
\textbf{HARTLEY Richard} and \textbf{ZISSERMAN Andrew},\,
\textit{Multiple View Geometry in Computer Vision},\, Cambridge,\,
Cambridge University Press,\, 2003 \\
\\
V\&GG,\, \textbf{Stereo Reconstruction [online]},\,
https://vgg.fiit.stuba.sk/2015-02/2783 (website visited on 18th
September 2019) \\
\\
ARMEA Albert,\, \textbf{Calculating a depth map from a stereo camera
  with OpenCV [online]}, \\
https://albertarmea.com/post/opencv-stereo-camera (website visited on
the 27th August 2019) \\
\\
ROSEBROCK Adrian (Pyimagesearch),\, \textbf{Ball Tracking with
  OpenCV [online]}, \\
https://www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv
(website visited on 21st February 2019) \\
\\
ROBER Mark,\, \textbf{Automatic Bullseye, MOVING DARTBOARD [online]}, \\
https://www.youtube.com/watch?v=MHTizZ\_XcUM (website visited on 26th
October 2019)
\end{document}