from imutils.video import VideoStream
import numpy as np
import cv2
import imutils
import time
from collections import namedtuple
import glob
from writecalib import read_calibration

lr, proj_l, proj_r, disparity_to_depth_map = read_calibration("values.pickle")

Pl = np.array(proj_l)
Pr = np.array(proj_r)

def rectify_pixel_coordinates(pixels):
    pixel_l, pixel_r = pixels
    disparity = pixel_l[0] - pixel_r[0]
    Q = np.array(disparity_to_depth_map)
    [[X], [Y], [Z], [W]] = Q.dot(np.array([[pixel_l[0]], [pixel_r[0]], [disparity], [1]]))
    threedpoint = np.array([[X/W], [Y/W], [Z/W], [1]])
    [[xl], [yl], [wl]] = Pl.dot(threedpoint)
    [[xr], [yr], [wr]] = Pr.dot(threedpoint)
    pixl = [xl/wl, yl/wl]
    pixr = [xr/wr, yr/wr]
    return [pixl, pixr]

def dist_calc(pixels):
    screenwidth = 3.68
    imagewidth = 600
    t = 4
    foc = 3.04
    pixel_l, pixel_r = rectify_pixel_coordinates(pixels)
    disparity = pixel_l[0] - pixel_r[0]
    dispmm = screenwidth * (disparity / imagewidth)
    return abs((foc * t) / dispmm)
