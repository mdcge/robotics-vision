# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2

# capture frames from the camera
def zzz(width=12, height=6):
    res = (width*128, height*128)
    # initialize the camera and grab a reference to the raw camera capture
    cameras = PiCamera(0, stereo_mode='side-by-side'), # PiCamera(1)
    for camera in cameras:
        camera.resolution = res
        camera.framerate = 32

    rawCapture = [PiRGBArray(camera, size=res) for camera in cameras]
    xxx = zip(*(camera.capture_continuous(rc, format="bgr", use_video_port=True)
                for rc, camera in zip(rawCapture, cameras)))

    # allow the camera to warmup
    time.sleep(0.1)

    for frames in xxx:
        # grab the raw NumPy array representing the image, then initialize the timestamp
        # and occupied/unoccupied text
        images = (f.array for f in frames)

        # show the frame
        for n, i in enumerate(images):
            yield i
            #cv2.imshow("Frame {}".format(n), i)

        # clear the stream in preparation for the next frame
        for rc in rawCapture:
            rc.truncate(0)
