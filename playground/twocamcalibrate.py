from imutils.video import VideoStream
import numpy as np
import cv2
import imutils
import time
from collections import namedtuple
import glob
import cameracalibrate as cc

# Termination criteria
stereo_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

objectpts = []
imagepts = []

def calibrate_cameras(objp, imgp_l, imgp_r, lcammat, ldistcoeffs, rcammat, rdistcoeffs, img_l, img_r):
    lsize = img_l[0].shape[::-1]
    rsize = img_r[0].shape[::-1]
    assert lsize == rsize
    return cv2.stereoCalibrate(objp, imgp_l, imgp_r, lcammat, ldistcoeffs, rcammat, rdistcoeffs, lsize, None, None, None, None, cv2.CALIB_FIX_INTRINSIC, stereo_criteria)

def rectify_cameras(lcammat, ldistcoeffs, rcammat, rdistcoeffs, img_l, img_r, rot, trans):
    lsize = img_l[0].shape[::-1]
    rsize = img_r[0].shape[::-1]
    assert lsize == rsize
    return cv2.stereoRectify(lcammat, ldistcoeffs, rcammat, rdistcoeffs, lsize, rot, trans, None, None, None, None, None, cv2.CALIB_ZERO_DISPARITY, alpha=1)

def calculate_calibrations(lr):
    (objectpts, imagepts_l, grey_l, img_l), (_, imagepts_r, grey_r, img_r) = cc.pre_calibration("lb", False), cc.pre_calibration("rb", False)
    (_, _, _, _, _, rotmat, transvect, _, _) = calibrate_cameras(objectpts, imagepts_l, imagepts_r, lr.l.cammatrix, lr.l.distcoeffs, lr.r.cammatrix, lr.r.distcoeffs, grey_l, grey_r)
    (rect_l, rect_r, proj_l, proj_r, disparity_to_depth_map, roi_l, roi_r) = rectify_cameras(lr.l.cammatrix, lr.l.distcoeffs, lr.r.cammatrix, lr.r.distcoeffs, grey_l, grey_r, rotmat, transvect)
    return proj_l, proj_r, disparity_to_depth_map
