import pickle
from collections import namedtuple
from imutils.video import VideoStream
import numpy as np
import cv2
import imutils
import time
from collections import namedtuple
import glob

# Object points: (0, 0, 0), (0, 0, 1), ..., (6, 5, 0), (6, 6, 0)
def object_points(n, m):
    objp = np.zeros((n*m,3), np.float32)
    objp[:,:2] = np.mgrid[0:m,0:n].T.reshape(-1,2)
    return objp

# Termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# Prepares image for corner-finding
def prep_image(image):
    img = cv2.imread(image)
    resized_img = cv2.resize(img, (960, 720))                    # Resize image
    img = resized_img
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return grey, img

# Constructing object and image points (with drawing option)
def img_pts(grey, img, corners, ret, draw=True):
    # cornerSubPix modifies 2nd argument (corners)
    cv2.cornerSubPix(grey, corners, (11, 11), (-1, -1), criteria)
    if draw:
        cv2.drawChessboardCorners(img, (7, 7), corners, ret)
        cv2.imshow('img', img)
        cv2.waitKey(0)
    print("Finished image")
    return corners

def pre_calibration(l_or_r_or_b, draw=True):
    # Picks all files in directory ending in '.jpg'
    images = sorted(glob.glob("*{}.jpg".format(l_or_r_or_b)))
    imagepts = map(imgp(draw), images)
    objectpts, imagepts, grey, img = zip(*imagepts)
    return objectpts, imagepts, grey, img

def imgp(draw=True):
    def imgp(image):
        print(image, end=" ... ")
        grey, img = prep_image(image)
        ret, corners = cv2.findChessboardCorners(grey, (7, 7), None)
        if ret:
            imagepts = img_pts(grey, img, corners, ret, draw)
            cv2.destroyAllWindows()
        return object_points(7, 7), imagepts, grey, img
    return imgp

def calibrate_camera(objp, imgp, img):
    imgsize = img[0].shape[::-1]
    return cv2.calibrateCamera(objp, imgp, imgsize, None, None)

def calib_camera_dist_0(ret, cammatrix, _, rvecs, tvecs):
    return calibcnst(ret, cammatrix, (0,0,0,0,0), rvecs, tvecs)

calibcnst = namedtuple('calibcnst', 'ret, cammatrix, distcoeffs, rvecs, tvecs')
LR = namedtuple('LR', 'l, r')

def calculate_calibration():
    (objectpts, imagepts_l, grey_l, img_l), (_, imagepts_r, grey_r, img_r) = pre_calibration("l", False), pre_calibration("r", False)
    l = calib_camera_dist_0(*calibrate_camera(objectpts, imagepts_l, grey_l))
    r = calib_camera_dist_0(*calibrate_camera(objectpts, imagepts_r, grey_r))
    return LR(l, r)
