"""Tracking.

Usage:
 tracking.py [-v <video>] [-s <shape>] [-S] [-w <width>] [-h <height>]

Options:
 -v <video>, --video=<video>     Path to the optional video file.
 -s <shape>, --shape=<shape>     Outline shape. [default=circle]
 -S, --stereo                    Stereo option.
 -w <width>, --width=<width>     Width. [default=12]
 -h <height>, --height=<height>  Height. [default=6]
"""

from imutils.video import VideoStream
import numpy as np
import cv2
import imutils
import time
from two_cams import zzz
from collections import namedtuple
import distcalc as dc

from docopt import docopt

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Tracking 0.1')

outline_colour = (0,255,0)

# Defining color boundaries of object to be tracked (HSV)

# Boundaries found using range-detector

ColourBounds = namedtuple("ColourBounds", "lo, hi")

# Without light shining at object
lego_redA = ColourBounds((0,92,76), (163,178,163))

# With natural light
lego_redB = ColourBounds((165, 59, 103), (192, 220, 181))

# With light shining at the object
lego_redC = ColourBounds((0,68,180), (182,175,225))

# With light shining at the object
tennis_ballA = ColourBounds((12, 48, 27), (58, 143, 238))

# Living room
tennis_ballB = ColourBounds((20, 70, 35), (150, 170, 250))

# With light shining at the object
pingpong_ballA = ColourBounds((105, 28, 117), (130, 91, 244))

def camerastream():
    vs = VideoStream(usePiCamera=True).start()
    # Warm up time for camera (or video file)
    time.sleep(2.0)
    while True:
        yield vs.read()

def filestream(videofile):
    vs = cv2.VideoCapture(videofile)
    time.sleep(2.0)
    while True:
        yield vs.read()

# If no file is specified, get reference to the camera
# If not, get reference to the video file

def args(d, k, default):
    return default if d[k] is None else int(d[k])

videofile = arguments["--video"]
width = args(arguments, "--width", 12)
height = args(arguments, "--height", 6)

if arguments["--stereo"]:
    vs = zzz(width=width, height=height)
elif videofile is None:
    # connect to the camera
    vs = camerastream()
else:
    vs = filestream(videofile)

# Warm up time for camera (or video file)
time.sleep(2.0)

def process_frames(source, process):
    for frame in source:
        yield process(frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break

def prepare_for_mask(frame):
    resized_frame = imutils.resize(frame, width=600)
    blurred = cv2.GaussianBlur(resized_frame, (11, 11), 0)
    hsv_frame = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    return hsv_frame, resized_frame

def make_mask(bounds):
    def make_mask(hsv_frame):
        mask = cv2.inRange(hsv_frame, bounds.lo, bounds.hi)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        return mask
    return make_mask

def find_contours(mask):
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    return cnts

def find_circle(cnts, frame, draw=True):
    if len(cnts) > 0:
        cnt = max(cnts, key=cv2.contourArea)
        circle = cv2.minEnclosingCircle(cnt)
        ((centrex, centrey), radius) = circle
        if radius > 5:
            cv2.circle(frame, (int(centrex), int(centrey)), int(radius), outline_colour, 2)
        return circle

def centre_circle(circle):
    if circle is None:
        return None
    ((centrex, centrey), _) = circle
    return (centrex, centrey)

def find_rectangle(cnts, frame, draw=True):
    if len(cnts) > 0:
        # Find the largest contour in the mask and use it to find the smallest bounding rectangle
        cnt = max(cnts, key=cv2.contourArea)
        rectangle = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rectangle)
        box = np.int0(box)
        (_, (w, h), _) = rectangle
        # Only draw the contour if width > 5 and height > 5
        if draw and w > 5 and h > 5:
            cv2.drawContours(frame, [box], 0, outline_colour, 2)
        return rectangle

def centre_rectangle(rectangle):
    if rectangle is None:
        return None
    ((x, y), _, _) = rectangle
    return (x, y)

def doitall(find_shape, bounds):
    make_colour_mask = make_mask(bounds)
    L, R = "Left", "Right"
    cv2.namedWindow(L)
    cv2.namedWindow(R)
    cv2.moveWindow(L, 10, 40)
    cv2.moveWindow(R, 615, 40)
    def doitall(frame):
        centres = []
        width = frame.shape[1] // 2
        frameL, frameR = frame[:, :width], frame[:, width:]
        for label, frame in zip((R,L), [frameL, frameR]):
            hsv_frame, resized_frame = prepare_for_mask(frame)
            mask = make_colour_mask(hsv_frame)
            cnts = find_contours(mask)
            shape = find_shape(cnts, resized_frame)
            centre = find_centre(shape)
            centres.append(centre)
            cv2.imshow(label, resized_frame)
        return centres
    return doitall

find_shape = find_rectangle if arguments["--shape"] == "rectangle" else find_circle
find_centre = centre_rectangle if arguments["--shape"] == "rectangle" else centre_circle

for shape in process_frames(vs, doitall(find_shape, tennis_ballB)):
    if None not in shape:
        print("{:4.0f}".format(dc.dist_calc(shape)))

# In case of stream, stop it; otherwise, release the camera

if videofile is None:
    vs.stop()
else:
    vs.release()

# Close all windows

cv2.destroyAllWindows()
