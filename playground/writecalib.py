import pickle
from cameracalibrate import calculate_calibration
from twocamcalibrate import calculate_calibrations
from sys import argv

def write_calibration(filename):
    with open(filename, "wb") as file:
        lr = calculate_calibration()
        pickle.dump(lr, file)
        pickle.dump(calculate_calibrations(lr), file)

def read_calibration(filename):
    with open(filename, "rb") as file:
        lr = pickle.load(file)
        proj_l, proj_r, disparity_to_depth_map = pickle.load(file)
    return lr, proj_l, proj_r, disparity_to_depth_map

if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: write_calib.py <filename>")
        raise SystemExit(1)
    write_calibration(argv[1])
